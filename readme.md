Hi.
This is a little multiprocessing example script for spawning processes from a shelve dict.



While you run mps.py, on a python console, you can work as exposed here:



stat = shelve.open('process_stat.dat') ; conf = shelve.open('config.dat') 

stat['i1'] = 'ps1'; stat['i2'] = 'ps2'; stat['i3'] = 'ps3' 

conf['i1'] = True ; conf['i2'] = False ; conf['i3'] = False 

stat.close() ; conf.close()




Once everything is set up and configured, you can choose to spawn\stop spawn processes by changing the conf relative process bool.
This script is so rude, there are better ways to deal with multiprocessing and process pools, but can help you understand how the
multiprocessing module works.


Thanks for reading.
