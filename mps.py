from __future__ import print_function
# module host #

import shelve
import multiprocessing
import time

config_file = "config.dat"
process_file = "process_stat.dat"

class Multips:
	def __init__(self):
		self.config_file = config_file
		self.process_file = process_file
		self.instances = {}

	def child_fn(self, id):
		file = open("files/{}.txt".format(id), "w")
		file.write(time.strftime("%H:%M:%S"))
		file.close()

	def host_fn(self):
		while True:
			self.config = shelve.open(self.config_file, flag='r')
			self.process = shelve.open(self.process_file, flag='r')
			print('reading stuff')
			for i in self.config:
				self.instances[i] = multiprocessing.Process(target=self.child_fn, 
				name=self.process[i], args=(self.process[i],) )
				print('ps defineds')
			for stat in self.config:
				if self.config[stat]:
					self.instances[stat].start()
			self.config.close()
			self.process.close()
			print('going to sleep')
			time.sleep(5)

if __name__ == "__main__":
	x = Multips()
	try: 
		yo = multiprocessing.Process(target=x.host_fn, name='host').start()
	except KeyboardInterrupt:
		break
